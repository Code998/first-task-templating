<!-- Footer -->
		<footer>
			<div class="contain">
				<div class="alamat satu">
					<h3>MALANG ( KANTOR PUSAT )</h3>
					<p>Jl. Soekarno Hatta Ruko Permata Griya Shanta NR. 24 – 25 <br> Malang – Jawa Timur <br> Telp: 0341 – 496 497, 488 890 <br> Fax: 0341 – 408 657 <br> Email: layanan@cendana2000.com</p>
				</div>
				<div class="alamat dua">
					<h3>JAKARTA ( KANTOR CABANG )</h3>
					<p>Jl. Kebagusan Raya no 192, Pasar Minggu, Jakarta Selatan 12550 <br> Telp: 021-22978922</p>
				</div>
			</div>
			<div class="cop">
				<hr>
				<p align="center">&copy;2018 PT. Cendana Teknika Utama. All Rights Reserved. </p>
			</div>
		</footer>
		<!-- End of Footer -->