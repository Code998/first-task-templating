<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Cendana extends CI_Controller {
	
		public function index()
		{
			$this->load->view('layout/header_b');
			$this->load->view('cendana2000/index.html');
			$this->load->view('layout/footer_a');
		}

		public function art_b()
		{
			$this->load->view('layout/header_b');
			$this->load->view('cendana2000/b_login/article.html');
			$this->load->view('layout/footer_a');
		}

		public function art_b1()
		{
			$this->load->view('layout/header_b');
			$this->load->view('cendana2000/b_login/article0.html');
			$this->load->view('layout/footer_a');
		}

		public function art_b2()
		{
			$this->load->view('layout/header_b');
			$this->load->view('cendana2000/b_login/article1.html');
			$this->load->view('layout/footer_a');
		}
		
		public function do_login()
		{
			$this->load->view('cendana2000/login.html');
		}

		public function a_login()
		{
			$this->load->view('layout/header_a');
			$this->load->view('cendana2000/a_login/index1.html');
			$this->load->view('layout/footer_a');
		}

		public function art_a()
		{
			$this->load->view('layout/header_a');
			$this->load->view('cendana2000/a_login/article.html');
			$this->load->view('layout/footer_a');
		}

		public function art_a1()
		{
			$this->load->view('layout/header_a');
			$this->load->view('cendana2000/a_login/article0.html');
			$this->load->view('layout/footer_a');
		}

		public function art_a2()
		{
			$this->load->view('layout/header_a');
			$this->load->view('cendana2000/a_login/article1.html');
			$this->load->view('layout/footer_a');
		}

		public function art_la()
		{
			$this->load->view('layout/header_adm');
			$this->load->view('cendana2000/a_login/list-article.html');
		}

		public function art_ag()
		{
			$this->load->view('layout/header_adm');
			$this->load->view('cendana2000/a_login/adm-gal.html');
		}

		public function art_aa()
		{
			$this->load->view('layout/header_adm');
			$this->load->view('cendana2000/a_login/add-art.html');
		}
	}
	
	/* End of file Cendana.php */
	/* Location: .//media/banana/3ABE16A5BE165A291/Programming/HTML and CSS/Tugas/tugas-7-ci-template/apps/controllers/Cendana.php */
?>